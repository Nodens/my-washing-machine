package com.gmas.mywashingmachine.services;

import org.androidannotations.annotations.EBean;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/*
 * Created by guillaume on 06/12/14.
 */
@EBean(scope = EBean.Scope.Singleton)
public class RestProvider<T> {

    private RestAdapter mRestAdapter;

    private static final String END_POINT = "";
    private static final String NAMESPACE = "";

    public RestProvider() {
        initAdapter(null);
    }

    public T getService(Class<T> service) {
        return mRestAdapter.create(service);
    }

    public void addHeader(final String headerName, final String headerValue) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(headerName, headerValue);
            }
        };

        initAdapter(requestInterceptor);
    }

    private void initAdapter(RequestInterceptor interceptor) {
        RestAdapter.Builder adapterBuilder = new RestAdapter.Builder().setEndpoint(END_POINT + NAMESPACE).setLogLevel(RestAdapter.LogLevel.FULL);

        if (interceptor != null) {
            adapterBuilder.setRequestInterceptor(interceptor);
        }

        mRestAdapter = adapterBuilder.build();
    }

}
