package com.gmas.mywashingmachine.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.gmas.mywashingmachine.R;

import org.androidannotations.annotations.EViewGroup;

/*
 * Created by guillaume on 06/12/14.
 */
@EViewGroup(R.layout.header_view)
public class HeaderView extends RelativeLayout {
    public HeaderView(Context context) {
        super(context);
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
