package com.gmas.mywashingmachine.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.method.Touch;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.gmas.mywashingmachine.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Created by guillaume on 26/11/14.
 */
@SuppressWarnings("ALL")
public class LinkTextView extends CustomTextView {

    private List<Hyperlink> listOfLinks = new ArrayList<Hyperlink>();

    private WeakReference<OnClickLinkListener> wr_listener;

    private Pattern hashtagsPattern = Pattern.compile("(#\\w+)");
    private Pattern screenNamesPattern = Pattern.compile("(#\\w+)");
    private Pattern hyperlinksPattern = Pattern.compile("([Hh][tT][tT][pP][sS]?://[^ ,'\">\\]\\)]*[^\\. ,'\">\\]\\)])");
    private Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
    private Pattern phonePattern = Pattern.compile("(0[1-68][-.\\s]?(\\d{2}[-.\\s]?){3}\\d{2})");


    private boolean mHashtagsEnabled = false;
    private boolean mScreenNamesEnabled = false;
    private boolean mHyperlinksEnabled = true;
    private boolean mEmailEnabled = true;
    private boolean mPhoneEnabled = true;

    private boolean mHitLink = false;
    private boolean mBlockConsumingNonUrlClicks = true;

    private int mLinkColor = Color.BLUE;

    public enum LinkType { HYPERLINK, SCREENNAME, HASHTAG, EMAIL, PHONE }

    public interface OnClickLinkListener {
        public void onClickLink(View textView, String link, LinkType type);
    }

    public LinkTextView(Context context) {
        super(context);
    }

    public LinkTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LinkTextView);

        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.LinkTextView_linkColor:
                    setLinkColor(a.getColor(attr, Color.BLUE));
                    break;
                case R.styleable.LinkTextView_linkHypertext:
                    enableHyperLinks(a.getBoolean(attr, true));
                    break;
                case R.styleable.LinkTextView_linkEmail:
                    enableEmailLinks(a.getBoolean(attr, true));
                    break;
                case R.styleable.LinkTextView_linkPhone:
                    enabledPhoneLinks(a.getBoolean(attr, true));
                    break;
                case R.styleable.LinkTextView_linkHashtag:
                    enableHashtagLinks(a.getBoolean(attr, false));
                    break;
                case R.styleable.LinkTextView_linkScreenname:
                    enableScreenNameLinks(a.getBoolean(attr, false));
                    break;
                default:
                    break;
            }
        }
        a.recycle();
    }

    public void enableHashtagLinks(boolean enable) {
        mHashtagsEnabled = enable;
    }

    void enableScreenNameLinks(boolean enable) {
        mScreenNamesEnabled = enable;
    }

    void enableHyperLinks(boolean enable) {
        mHyperlinksEnabled = enable;
    }

    void enableEmailLinks(boolean enable) {
        mEmailEnabled = enable;
    }

    void enabledPhoneLinks(boolean enable) {
        mPhoneEnabled = enable;
    }

    void setLinkColor(int color) {
        mLinkColor = color;
    }

    public void setTextAndActivateLinks(String text) {
        listOfLinks.clear();

        SpannableString linkableText = new SpannableString(text);

        if (mHashtagsEnabled) {
            gatherLinks(linkableText, hashtagsPattern);
        }

        if (mScreenNamesEnabled) {
            gatherLinks(linkableText, screenNamesPattern);
        }

        if (mHyperlinksEnabled) {
            gatherLinks(linkableText, hyperlinksPattern);
        }

        if(mEmailEnabled) {
            gatherLinks(linkableText, emailPattern);
        }

        if(mPhoneEnabled) {
            gatherLinks(linkableText, phonePattern);
        }

        for (Hyperlink linkSpec : listOfLinks) {
            linkableText.setSpan(linkSpec.span, linkSpec.start, linkSpec.end, 0);
        }

        MovementMethod m = getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (getLinksClickable()) {
                setMovementMethod(LocalLinkMovementMethod.getInstance());
            }
        }
        setText(linkableText);
    }

    @Override
    public boolean hasFocusable() {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mHitLink = false;
        boolean res = super.onTouchEvent(event);

        if (mBlockConsumingNonUrlClicks)
            return mHitLink;

        return res;
    }

    public void setOnClickLinkListener(OnClickLinkListener listener) {
        wr_listener = new WeakReference<OnClickLinkListener>(listener);
    }

    private void gatherLinks(Spannable s, Pattern pattern) {
        Matcher m = pattern.matcher(s);

        LinkType type = LinkType.HYPERLINK;

        if (pattern.equals(screenNamesPattern)) {
            type = LinkType.SCREENNAME;
        }
        else if (pattern.equals(hashtagsPattern)) {
            type = LinkType.HASHTAG;
        }
        else if (pattern.equals(emailPattern)) {
            type = LinkType.EMAIL;
        }
        else if (pattern.equals(phonePattern)) {
            type = LinkType.PHONE;
        }

        while (m.find()) {
            int start = m.start();
            int end = m.end();

            Hyperlink link = new Hyperlink();

            link.type = type;
            link.textSpan = s.subSequence(start, end);
            link.color = mLinkColor;
            link.span = new InternalURLSpan(link.textSpan.toString(), link.type, link.color);
            link.start = start;
            link.end = end;

            listOfLinks.add(link);
        }
    }

    /*
     * This is class which gives us the clicks on the links which we then can
     * use.
     */
    public class InternalURLSpan extends ClickableSpan {

        private String clickedSpan;
        private LinkType clickedType;
        private int clickedColor;

        public InternalURLSpan(String span, LinkType type, int color) {
            clickedSpan = span;
            clickedType = type;
            clickedColor = color;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.bgColor = Color.TRANSPARENT;
            ds.setColor(clickedColor);
            ds.setUnderlineText(true);
        }

        @Override
        public void onClick(View textView) {
            OnClickLinkListener listener = wr_listener.get();
            if (listener != null) {
                listener.onClickLink(textView, clickedSpan, clickedType);
            }
        }
    }

    public static class LocalLinkMovementMethod extends LinkMovementMethod {
        static LocalLinkMovementMethod sInstance;


        public static LocalLinkMovementMethod getInstance() {
            if (sInstance == null)
                sInstance = new LocalLinkMovementMethod();

            return sInstance;
        }

        @Override
        public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                x -= widget.getTotalPaddingLeft();
                y -= widget.getTotalPaddingTop();

                x += widget.getScrollX();
                y += widget.getScrollY();

                Layout layout = widget.getLayout();
                int line = layout.getLineForVertical(y);
                int off = layout.getOffsetForHorizontal(line, x);

                ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);

                if (link.length != 0) {
                    if (action == MotionEvent.ACTION_UP) {
                        link[0].onClick(widget);
                    }
                    else if (action == MotionEvent.ACTION_DOWN) {
                        Selection.setSelection(buffer, buffer.getSpanStart(link[0]), buffer.getSpanEnd(link[0]));
                    }

                    if (widget instanceof LinkTextView){
                        ((LinkTextView) widget).mHitLink = true;
                    }
                    return true;
                }
                else {
                    Selection.removeSelection(buffer);
                    Touch.onTouchEvent(widget, buffer, event);
                    return false;
                }
            }
            return Touch.onTouchEvent(widget, buffer, event);
        }
    }

    /*
     * Class for storing the information about the Link Location
     */
    class Hyperlink {

        LinkType type;
        CharSequence textSpan;
        InternalURLSpan span;
        int start;
        int end;
        int color;
    }


}
