package com.gmas.mywashingmachine.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmas.mywashingmachine.R;
import com.gmas.mywashingmachine.models.Item;

import java.util.List;

/*
 * Created by guillaume on 06/12/14.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<Item> mItems;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView mTitleItem;
        public TextView mSubtitleItem;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
            mTitleItem = (TextView) v.findViewById(R.id.mTitleItem);
            mSubtitleItem = (TextView) v.findViewById(R.id.mSubtitleItem);
        }
    }

    public RecyclerAdapter (List<Item> items) {
        mItems = items;
    }

    public void addItems(List<Item> items, int position) {
        if (mItems != null && items != null) {
            mItems.addAll(position, items);
        }
    }

    public void addItem(Item item, int position) {
        if(mItems != null && item != null) {
            mItems.add(position, item);
        }
    }

    public void clearItems() {
        if (mItems != null) {
            mItems.clear();
        }
    }

    public void removeItem(int position) {
        if (mItems != null) {
            mItems.remove(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, parent, false);
        return new ViewHolder((CardView) v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTitleItem.setText(mItems.get(position).getTitle());
        holder.mSubtitleItem.setText(mItems.get(position).getSubTitle());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}
