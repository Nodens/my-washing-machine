package com.gmas.mywashingmachine.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by guillaume on 06/12/14.
 */
@EBean
public abstract class AbstractAdapter<T> extends BaseAdapter {

    @RootContext
    protected Context mCtx;

    protected int mSelectedPosition = 0;
    protected List<T> mItems = new ArrayList<T>();

    protected static final String TAG = "Adapter";

    public boolean isNotEmpty() {
        return (mItems != null) && (mItems.size() > 0);
    }

    public int getSelectedPosition() {
        return mSelectedPosition;
    }

    public void setSelectedPosition(int mSelectedPosition) {
        this.mSelectedPosition = mSelectedPosition;
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return mItems;
    }

    public void setItems(List<T> items) {
        mItems = items;
    }

    public void addItem(T item, int position) {
        if (mItems != null && item != null) {
            mItems.add(position, item);
        }
    }

    public void addItems(List<T> items, int position) {
        if (mItems != null && items != null) {
            mItems.addAll(position, items);
        }
    }

    public void clearItems() {
        if (mItems != null) {
            mItems.clear();
        }
    }

    public void removeItem(int position) {
        if (mItems != null) {
            mItems.remove(position);
        }
    }

    @Override
    public int getCount() {
        return isNotEmpty() ? mItems.size() : 0;
    }

    @Override
    public T getItem(int position) {
        return isNotEmpty() ? mItems.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);


}
