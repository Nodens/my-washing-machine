package com.gmas.mywashingmachine.fragments;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gmas.mywashingmachine.R;
import com.gmas.mywashingmachine.adapters.RecyclerAdapter;
import com.gmas.mywashingmachine.models.Item;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by guillaume on 06/12/14.
 */
@EFragment(R.layout.home_view)
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @ViewById
    RecyclerView mRecyclerView;

    @ViewById
    SwipeRefreshLayout mSwipeContainer;

    private RecyclerAdapter mAdapter;

    @AfterViews
    void init() {

        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorScheme(R.color.swipe1,
                R.color.swipe2,
                R.color.swipe3,
                R.color.swipe4);

        List<Item> items = new ArrayList<Item>();

        Item item = new Item();
        item.setTitle("Machine Head");
        item.setSubTitle("Heavy Métal");
        items.add(item);
        Item item1 = new Item();
        item1.setTitle("Rammstein");
        item1.setSubTitle("Métal Fusion");
        items.add(item1);
        Item item2 = new Item();
        item2.setTitle("Deftones");
        item2.setSubTitle("Rock Alternatif");
        items.add(item2);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new RecyclerAdapter(items);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                if(mAdapter.getItemCount() < 6) {
                    List<Item> items = new ArrayList<Item>();

                    Item item = new Item();
                    item.setTitle("Rage Against the Machine");
                    item.setSubTitle("Hard Rock");
                    items.add(item);
                    Item item1 = new Item();
                    item1.setTitle("Muse");
                    item1.setSubTitle("Pop Rock");
                    items.add(item1);
                    Item item2 = new Item();
                    item2.setTitle("Uncommenfrommars");
                    item2.setSubTitle("Punk");
                    items.add(item2);

                    mAdapter.addItems(items, mAdapter.getItemCount());
                }

                mSwipeContainer.setRefreshing(false);
            }
        }, 5000);
    }

    @Click(R.id.fab_add)
    void onClickAddItem() {
        Item item = new Item();
        item.setTitle("NOFX");
        item.setSubTitle("Punk");
        mAdapter.addItem(item, mAdapter.getItemCount());
        mAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(mAdapter.getItemCount());
    }
}
