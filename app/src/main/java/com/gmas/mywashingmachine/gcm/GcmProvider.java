package com.gmas.mywashingmachine.gcm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.gmas.mywashingmachine.R;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.IOException;
import java.lang.ref.WeakReference;

/*
 * Created by guillaume on 06/12/14.
 */
@EBean
public class GcmProvider {

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "GcmProvider";

    @RootContext
    Activity mCtxt;

    private WeakReference<IGcmListener> wr_listener;

    public interface IGcmListener {
        public void onRegister(String registerId);
    }

    public void setListener(IGcmListener listener) {
        wr_listener = new WeakReference<IGcmListener>(listener);
    }

    public boolean isRegistered() {
        return !getRegistrationId().isEmpty();
    }

    public void register(String projectNumberId) {
        if (checkPlayServices() && !isRegistered()) {
            registertoGcm(projectNumberId);
        }
    }

    /*
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mCtxt);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, mCtxt, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "GooglePlayServices not supported for this device.");
            }
            return false;
        }
        return true;
    }

    private SharedPreferences getGCMPreferences() {
        return mCtxt.getSharedPreferences(mCtxt.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    /*
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    public String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(mCtxt);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /*
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /*
     * Stores the registration ID and app versionCode in the application's
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(mCtxt);
        Log.i(TAG, "Saving regId " + regId + " on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }

    @Background
    void registertoGcm(String projectNumberId) {
        try {
            String regid = GoogleCloudMessaging.getInstance(mCtxt).register(projectNumberId);
            storeRegistrationId(regid);
            if (wr_listener != null && wr_listener.get() != null) {
                wr_listener.get().onRegister(regid);
            }
        } catch (IOException ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

}

