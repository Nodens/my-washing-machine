package com.gmas.mywashingmachine.models;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/*
 * Created by guillaume on 06/12/14.
 */
public class Item implements Serializable {

    private String title;
    private String subTitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
